# Documentação

- [Mind Map](https://gitlab.com/BDAg/Chat-bot/wikis/mind%20map)
 - [Project Charter](https://gitlab.com/BDAg/Chat-bot/wikis/Project-Charter)
 - [Cronograma](https://gitlab.com/BDAg/Chat-bot/wikis/cronograma)
 - [Arquitetura da Solução](https://gitlab.com/BDAg/Chat-bot/wikis/arquitetura-da-solu%C3%A7%C3%A3o)
 - [MVP](https://gitlab.com/BDAg/Chat-bot/wikis/MVP)
 - [Montagem do desenvolvimento do Ambiente](https://gitlab.com/BDAg/Chat-bot/wikis/ambiente/edit)
 - [Arquitetura Banco de Dados](https://gitlab.com/BDAg/Chat-bot/wikis/arquitetura-banco-de-dados)
 - [Mapa de Definição Tecnologica](https://gitlab.com/BDAg/Chat-bot/wikis/mapa-tecnologico-de-conhecimento)
 - [Lista de Funcionalidades](https://gitlab.com/BDAg/Chat-bot/wikis/lista-de-funcionalidades)
 - [Cronograma com os responsáveis pelas tarefas](https://gitlab.com/BDAg/Chat-bot/wikis/cronograma-com-os-respons%C3%A1veis-pelas-tarefas)
 - [Codigo do Chat Bot](https://gitlab.com/BDAg/Chat-bot/wikis/c%C3%B3digo-do-chat-bot)
 - [Documentação do Chat Bot](https://gitlab.com/BDAg/Chat-bot/wikis/Documenta%C3%A7%C3%A3o-do-Chat-Bot)
## Chat-bot
### Surgiu pela necessidade de responder clientes via chat.
### A finalidade é ganhar velocidade e gerar dados para posterior analise de Big Data.

## Equipe:

- [Vitor Tedesco (CTO)](https://gitlab.com/BDAg/Chat-bot/wikis/Vitor-Tedesco)
- [Bruno Morini (CTO-aprendiz)](https://gitlab.com/BDAg/Chat-bot/wikis/Bruno-Morini)
- [Ariane](https://gitlab.com/BDAg/Chat-bot/wikis/Ariane)
- [Elaine](https://gitlab.com/BDAg/Chat-bot/wikis/Elaine)
- [Ezequiel](https://gitlab.com/BDAg/Chat-bot/wikis/Ezequiel-Pontes)
- [Thiago](https://gitlab.com/BDAg/Chat-bot/wikis/Thiago)
- [Jose Zanguettin](https://gitlab.com/BDAg/Chat-bot/wikis/Jose-Zanguettin)
- [Natalia](https://gitlab.com/BDAg/Chat-bot/wikis/Natalia)
- [Pesquisas realizadas pela equipe](https://gitlab.com/BDAg/Chat-bot/wikis/pesquisas-realizadas-pela-equipe)
